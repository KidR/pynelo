import argparse
import json
from argparse import ArgumentError
from pathlib import Path
from lib.manganelo.conf import MangaDirectory, dict_conf
from lib.manganelo.api import ManganeloAPI

class PyneloArgs:

    def __init__(self):
        self.__parser = argparse.ArgumentParser(prog='PyNelo',description="A simple command-line app which download scans from manganelo.com for a given url")
        self.__parser.add_argument('url',nargs='?')
        self.__range()
        self.__set_numbers()
        self.__inverse()
        self.__force()
        self.__timesleep()
        self.__import_file()
        self.__quit_vpn()
        self.__clear_log()
        self.__update()
        self.__show()
        self.__filter()
        self.__search()

    def __range(self):
        self.__parser.add_argument('-r','--range',nargs='+',help="define chapter's range (int or double) to download.",default=None)

    def __set_numbers(self):
        self.__parser.add_argument('-s','--set',nargs='+',help="download only chapters numbers in set.",default=None)

    def __inverse(self):
        self.__parser.add_argument('-I','--inverse', action='store_true',help="download every chapters numbers except those in set.",default=False)

    def __force(self):
        self.__parser.add_argument('-f','--force', action='store_true',help='force a download if the manga directory is not empty.', default=False)

    def __timesleep(self):
        self.__parser.add_argument('-t','--time',nargs=1,help="sleep <seconds> between each chapter's download.",default=[0])

    def __import_file(self):
        self.__parser.add_argument('-i','--import-file',nargs=1,help="import <datafile> which contains all MANGANELO URL to download.",default=None)
    
    def __quit_vpn(self):
        self.__parser.add_argument('-q','--quit-vpn',action='store_true',help='ignore vpn config from configuration file.',default=False)
    
    def __clear_log(self):
        self.__parser.add_argument('-C','--clear-log',action='store_true',help='clear log file if exists in configuration file.',default=False)

    def __update(self):
        self.__parser.add_argument('-u','--update', nargs=1, help='update manga by downloading the latest missing chapters.',default=None)

    def __show(self):
        self.__parser.add_argument('--show', action='store_true',help='show all downloaded mangas.',default=None)

    def __filter(self):
        self.__parser.add_argument('--filter', nargs=1, help='show downloaded mangas if corresponding to <genre> filter.',default=None)
    
    def __search(self):
        self.__parser.add_argument('--search', nargs='+', help='search <title> manga on manganelo.com',default=None)

    def parse(self,args=None):

        def print_manga(title,genres,width=65):
            print(f"> {title}",end='')
            print(" "*(width-len(title)),", ".join(genres))

        def get_mangas(mangas:Path):
            return sorted(filter(MangaDirectory.is_manga_dir,Path(mangas).iterdir()))

        self.__parser.parse_args(args,namespace=self) if args else self.__parser.parse_args(namespace=self)
        """ Exceptions part """
        if not(self.url or self.import_file or self.update or self.show or self.filter or self.search):
            raise ArgumentError(self.url,'***** Must contains manganelo.com url.')
        if self.import_file and (self.url or self.update) or self.update and (self.url or self.import_file):
            raise ArgumentError(None,'***** Must be either datafile or manga to update or a manganelo.com url.')
        if self.range and self.set:
            raise ArgumentError(None,"***** Can't use --range and --set options in the same command line.")
        if self.inverse and not self.set:
            raise ArgumentError(self.inverse,"***** Can't use --inverse option without --set.")
        if self.show and self.filter:
            raise ArgumentError(None,"***** Can't use --show and --filter in the same command line.")
        if self.show  and self.url:
            raise ArgumentError(None,"***** When using --show, there is no url.")
        if self.filter  and self.url:
            raise ArgumentError(None,"***** When using --filter, there is no url.")
        if self.search and len(self.search) > 2:
            raise ArgumentError(None,"***** <title> [<page>] is expected when --search.")
        """ Parsing part """
        if self.range:
            if len(self.range) not in (1,2):
                raise ArgumentError(None,"***** Must be 1 or 2 range.")
            self.range = list(map(float,self.range))
        if self.set:
            self.set = set(map(float,self.set))
        if self.import_file:
            self.import_file = open(Path(self.import_file[0]),'r')
        if self.update:
            if self.update[0].lower() == 'all':
                self.update = []
                for manga in Path(dict_conf['mangas_directory']).iterdir():
                    if manga.is_dir() and not manga.name.startswith(".") :
                        self.update.append(json.load(open(manga / MangaDirectory.INFO ,'r')))
            else:
                self.update = [json.load(open(Path(self.update[0]) / MangaDirectory.INFO ,'r'))]
        self.time = int(self.time[0])
        if self.show:
            def to_show():
                for manga in get_mangas(dict_conf['mangas_directory']):
                    with open(manga / MangaDirectory.INFO,'r') as info:
                        dict_info = json.load(info)
                        print_manga(dict_info['title'],dict_info['genres'])                
            self.show = to_show
        if self.filter:
            def to_filter(genre):
                for manga in get_mangas(dict_conf['mangas_directory']):
                    with open(manga / MangaDirectory.INFO,'r') as info:
                        dict_info = json.load(info)
                        if genre in map(str.lower,dict_info['genres']):
                            print_manga(dict_info['title'],dict_info['genres'])
            self.filter = {'func': to_filter,'value': self.filter[0].lower().replace('_',' ')}
        if self.search:
            def display_results(results:dict,page:int):
                print("+"*10,f"PAGE {page}","+"*10)
                for i in range(len(results)):
                    print(f"#{(i+1)} {results[i]['title']}")
                print("+"*10,f"PAGE {page}","+"*10)

            def read_choice():
                choice = ''
                while choice not in ['p','n','0','r'] and not str.isdigit(choice):
                    choice = input("> Number #N to download, 'p' previous page, 'n' next page, 'r' to refresh, 0 to exit: ")
                return choice.lower()

            page = 1 if len(self.search) == 1 else int(self.search[1])
            while "I did not chose an option":
                results = ManganeloAPI.search(title=self.search[0],page=page).parse()
                display_results(results,page)
                choice = read_choice()
                if choice == '0':
                # exit
                    self.search = 0
                    break
                elif choice == 'p' and page > 1:
                # previous
                    page -= 1
                elif choice == 'n':
                # next
                    page += 1
                elif choice == 'r':
                    continue
                elif choice.isdigit() and int(choice) > 0:
                # download N
                    self.search = results[int(choice)-1]['url']
                    break
            