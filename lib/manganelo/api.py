import requests as req
import re
from urllib.error import URLError
from bs4 import BeautifulSoup

class ManganeloAPI:

    """Manganelo API useful constants"""
    all_chapters_div = '.chapter-name.text-nowrap'
    chapters_div = '.container-chapter-reader'
    panel_div = '.panel-breadcrumb'
    table_value = '.table-value'
    search_item = '.search-story-item'
    last_page = '.page-blue.page-last'
    info_image = '.info-image'
    pattern_chapter_url = 'https://manganelo.com/chapter/'
    pattern_manga_url = 'https://manganelo.com/manga/'
    pattern_search_url = 'https://manganelo.com/search/'
    MAX_MANGA_PER_PAGE = 20
    HEADER_FIREFOX = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:76.0) Gecko/20100101 Firefox/76.0"}

    """
    Get manganelo.com html source code and parse it into structured information
    """
    def __init__(self,url):
        if isinstance(url,str) and (ManganeloAPI.is_correct_manga_url(url) or 
            ManganeloAPI.is_correct_chapter_url(url) or
            ManganeloAPI.is_correct_search_url(url)):
            self.__result = req.get(url,headers=ManganeloAPI.HEADER_FIREFOX)
            self.__html = self.__result.text
            self.__soup = BeautifulSoup(self.__html, 'html.parser')
            while self.is_404_not_found():
                self.__init__(self.__result.url)
        else:
            raise URLError('***** Bad manganelo.com url. Must be MANGA or CHAPTER type.')

    """Get manganelo URL

    Returns:
        str -- manga or chapter url from manganelo.com
    """
    @property
    def url(self):
        return self.__result.url

    @staticmethod
    def __to_format(string:str):
        return string.strip()

    """Get manga title from MANGA or CHAPTER type url

    Returns:
        str -- manga title
    """
    @property
    def title(self):
        return ManganeloAPI.__to_format(self.__soup.select(f'{ManganeloAPI.panel_div} > a')[1].string)

    """Get manga genres

    Returns:
        list -- list of genres
    """
    @property
    def genres(self):
        row_genres = self.__soup.select(f'{ManganeloAPI.table_value}')[-1]
        return [ a.string for a in row_genres.find_all('a') ]

    """Get chapter from URL 

    Returns:
        str -- chapter
    """
    @property
    def chapter(self):
        if ManganeloAPI.is_correct_chapter_url(self.url):
            return self.url.split('/')[-1]

    """Get id from URL 

    Returns:
        str -- id
    """
    @property
    def manga_id(self):
        if ManganeloAPI.is_correct_chapter_url(self.url):
            return self.url.split('/')[-2]
        elif ManganeloAPI.is_correct_manga_url(self.url):
            return self.url.split('/')[-1]

    """Return chapter number from URL which pattern is
    https://manganelo.com/chapter/xxx/chapter_#

    Returns:
        str -- chapter number
    """
    @property
    def chapter_number(self):
        return self.chapter.split('_')[-1]

    @classmethod
    def get_chapter_number(cls,url):
        if ManganeloAPI.is_correct_chapter_url(url):
            return url.split('/')[-1].split('_')[-1]
    
    @property
    def manga_img(self):
        if ManganeloAPI.is_correct_manga_url(self.url):
            return self.__soup.select(f'{ManganeloAPI.info_image} > img')[0]['src']

    """Get all scans images link from chapter

    Returns:
        dict -- all scans links from a chapter
    """
    def parse(self):

        def extract_scan_name(url):
            return url.split('/')[-1]

        if ManganeloAPI.is_correct_chapter_url(self.url):
            scans = { extract_scan_name(img['src']): img['src'] for img in self.__soup.select(f'{ManganeloAPI.chapters_div} > img') }
            return scans
        elif ManganeloAPI.is_correct_manga_url(self.url):
            chapters = [ a['href'] for a in self.__soup.select(f'{ManganeloAPI.all_chapters_div}')]
            return chapters
        elif ManganeloAPI.is_correct_search_url(self.url):
            results = [{'title':a['title'],'url':a['href']} for a in self.__soup.select(f'{ManganeloAPI.search_item} > .item-img')]
            return results
        else:
            raise ValueError('*****Must be an url from manganelo.com type CHAPTER.')

    

    """Check if an url is a chapter from manganelo.com
    
    Returns:
        bool -- if it's chapter url from manganelo.com
    """
    @classmethod
    def is_correct_chapter_url(cls,url):
        return url.startswith(ManganeloAPI.pattern_chapter_url)
    
    """Check if an url is a manga from manganelo.com

    Returns:
        bool -- if it's manga url from manganelo.com
    """
    @classmethod
    def is_correct_manga_url(cls,url):
        return  url.startswith(ManganeloAPI.pattern_manga_url)

    """Check if an url is the search functionnality of manganelo.com

    Returns:
        bool -- if it's the search functionnality of manganelo.com
    """
    @classmethod
    def is_correct_search_url(cls,url):
        return  url.startswith(ManganeloAPI.pattern_search_url)

    """Check if manganelo.com has a 404 problem

    Returns:
        bool -- if manganelo.com is in trouble
    """
    def is_404_not_found(self):
        return '404 Not Found' in self.__soup.title.string

    """HTML source code from manganelo.com

    Returns:
        str -- manganelo.com html source code
    """
    def __repr__(self):
        return self.__html

    """Search a manga from its title on manganelo.com

    Arguments:
        title - manga title 
    """
    @classmethod
    def search(cls,title,page=1):
        title = re.sub(r"[^ a-zA-Z0-9]",r"",title).strip() # delete all metachars
        title = re.sub(r"\s+","_",title).strip('_') # replace space by _ except at the end or beginning
        return cls(url=f'{ManganeloAPI.pattern_search_url}/{title}?page={page}')


if __name__ == "__main__":
    obj = ManganeloAPI('https://manganelo.com/manga/oe920828')
    print(obj.manga_img)