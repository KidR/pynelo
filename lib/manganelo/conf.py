import json
import time
import os
import re
import subprocess as proc
from pathlib import Path
from types import GeneratorType

with open('./conf.json','r') as file_conf:
    dict_conf =  json.load(file_conf)

    class FileLogger:

        MAX_LOG_SIZE = 1000000

        __instance = None

        @staticmethod
        def today():
            return time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())

        @staticmethod
        def get_log():
            FileLogger()
            return FileLogger.__instance

        def __init__(self,path=dict_conf['logfile']):
            if not FileLogger.__instance:
                FileLogger.__instance = self
                self.__log = open(path,'a')
                if self.__log.seek(0,os.SEEK_END) >= FileLogger.MAX_LOG_SIZE:
                    self.clear

        def regular(self,msg):
            self.__log.write(f'{self.today()}> {msg}\n')
        
        def error(self,msg):
            self.__log.write(f'{self.today()}****** {msg}\n')

        def clear(self):
            self.__log.truncate(0)
        

    class MangaDirectory:

        DIR_MANGAS = dict_conf['mangas_directory']

        CONF = "conf.json"

        INFO = ".info"

        """Create manga directory and info file

        Arguments:
            manga -- manga name
        """
        def __init__(self,manga):
            self.__log = FileLogger.get_log()
            if isinstance(manga,str):
                self.__dir = Path(MangaDirectory.DIR_MANGAS) / MangaDirectory.__to_title(manga)
                self.__info = self.__dir / MangaDirectory.INFO
                self.__dict_info = {'title': manga}
                if not self.__dir.exists():
                    self.__log.regular(f'CREATE MANGA {self.__dir.__str__()}')
                    self.__dir.mkdir()
                    self.__generate_info()
                    self.__generate_webviewer()
                self.__content = list(self.__dir.iterdir())

        def __str__(self):
            return self.__dir.__str__()

        @property
        def is_free(self):
            return len(self.__content) <= 2

        @property
        def path(self):
            return self.__dir

        @classmethod
        def __to_title(cls,title):
            title = title.lower().strip().replace(' ','_')
            return re.sub(r"[~?',!()]",r"",title)

        def update_info(self):
            info = open(self.__info.__str__(),'r')
            new_info = json.load(info)
            new_info.update(self.__dict_info)
            info = open(self.__info.__str__(),'w')
            info.write(json.dumps(new_info))
            info.close()
                
        def add_info(self,key,value):
            self.__dict_info[key] = value

        def get_info(self,key):
            with open(self.__info.__str__(),'r') as info:
                info = json.load(info)
                return info[key] if info.get(key) else None

        @classmethod
        def is_empty(cls,content):
            if isinstance(content,Path):
                content = content.iterdir()
            if isinstance(content,GeneratorType):
                content = list(content)
            return len(content) == 0
        
        @classmethod
        def is_manga_dir(cls,dir:Path):
            return '.git' not in str(dir) and Path.is_dir(dir)

        def __generate_info(self):
            with open(self.__info.__str__(),'w') as info:
                self.__log.regular(f'GENERATE INFO {self.__info.__str__()}')
                info.write(json.dumps(self.__dict_info))

        def __generate_webviewer(self):
            src = Path(f"{dict_conf['viewer']['directory']}/{dict_conf['viewer']['template']}")
            dest = self.__dir / 'index.php'
            self.__log.regular(f'GENERATE WEBVIEWER {dest}')
            proc.call(f'ln {src} {dest}'.split())

        def create_chapter(self,chapter):
            dir_chapter = self.__dir / chapter
            self.__log.regular(f'CREATE CHAPTER {dir_chapter}')
            dir_chapter.mkdir(exist_ok=True)
            return dir_chapter

        def __len__(self):
            return len(self.__content)
