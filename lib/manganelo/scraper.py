import requests as req
import json
import time
import shutil
from pathlib import Path
from lib.manganelo.api import ManganeloAPI
from lib.manganelo.conf import MangaDirectory
from lib.manganelo.conf import FileLogger

class ManganeloScraper:

    MANGA_IMG_NAME = "cover.jpg"

    def __init__(self,url=None):
        if url and isinstance(url,str):
            self.__api = ManganeloAPI(url)
            self.__manga_dir = MangaDirectory(self.__api.title)
            self.__url = url
        self.__last_chapter = self.__manga_dir.get_info('last_chapter')
        if not self.__last_chapter:
            self.__last_chapter = 0
        self.__log = FileLogger.get_log()

    """Download an image from a website
    """
    def __download_image(self,url,name):
        img_data = req.get(url,stream=True,headers=ManganeloAPI.HEADER_FIREFOX)
        if img_data.status_code != 200:
            self.__log.error(f"ERROR {img_data.status_code} AT {url} ")
            if isinstance(self.__errors,list):
                self.__errors.append(name.name)
        else:
            with open(f'{name}','wb') as image:
                shutil.copyfileobj(img_data.raw,image) 

    def __download_chapter_scan(self,force=False):
        chapter = self.__manga_dir.create_chapter(self.__api.chapter)
        if not MangaDirectory.is_empty(chapter) and not force:
            print(f'> ... aborting {self.__api.url}  => has non-empty chapter directory')
            self.__log.regular(f"ABORT DOWNLOAD {self.__api.url}")
        else:
            self.__errors = []
            print(f'> ... downloading {self.__api.url}')
            for name, url in self.__api.parse().items():
                self.__log.regular(f"{'FORCE ' if force else ''} DOWNLOAD SCAN {chapter}/{name}")
                self.__download_image(url,name=chapter / name)
            self.__log.regular(f"CHAPTER COMPLETED {chapter}")
            if len(self.__errors) > 0:
                with open(chapter / 'errors.json','w') as errors:
                    errors.write(json.dumps(self.__errors,indent=2))

    def __process_chapter(self,force=False,sleep=0):
        while self.__api.is_404_not_found():
            self.__log.error("404 NOT FOUND: Reloading {self.__api.url}")
            self.__api = ManganeloAPI(self.__api.url)
        self.__download_chapter_scan(force=force)
        self.__last_chapter = max(self.__last_chapter,float(self.__api.chapter_number))
        if float.is_integer(float(self.__last_chapter)):
            self.__last_chapter = int(self.__last_chapter)
        if sleep > 0:
            self.__log.regular(f"IS SLEEPING FOR {sleep} s.")
            time.sleep(sleep)


    def __process_range_chapters(self,range,force=False,sleep=0):
        chapters = self.__api.parse()
        epsilon = 0.001
        for chapter in chapters:
            self.__api = ManganeloAPI(chapter)
            chapter_num = float(self.__api.chapter_number)
            interval_condition = len(range) == 2 and chapter_num <= range[0] and chapter_num >= range[-1]
            single_condition = len(range) == 1 and chapter_num >= (range[0]+epsilon)
            if interval_condition or single_condition:
                self.__process_chapter(force=force,sleep=sleep)
            elif (len(range) == 2 and chapter_num < range[-1]) or (len(range) == 1 and chapter_num < range[0]):
                break
            
    def __process_set_chapters(self,set_numbers,force=False,sleep=0,inverse=False):
        chapters = self.__api.parse()
        if not inverse:
            foo = lambda x: float(ManganeloAPI.get_chapter_number(x)) in set_numbers
        else:
            foo = lambda x: float(ManganeloAPI.get_chapter_number(x)) not in set_numbers
        chapters = list(filter(foo, chapters))
        for chapter in chapters:
            self.__api = ManganeloAPI(chapter)
            self.__process_chapter(force=force,sleep=sleep)

    def __process_all_chapters(self,range=None,inverse=False,set_numbers=None,force=False,sleep=0):
        chapters = self.__api.parse()
        for chapter in chapters:
            self.__api = ManganeloAPI(chapter)
            self.__process_chapter(force=force,sleep=sleep)

    def run(self,force=False,range=None,set_numbers=None,inverse=False,sleep=0):
        if ManganeloAPI.is_correct_chapter_url(self.__url):
            self.__manga_dir.add_info('url',f'{ManganeloAPI.pattern_manga_url}{self.__api.manga_id}')
            self.__process_chapter(force=force,sleep=sleep)
        elif ManganeloAPI.is_correct_manga_url(self.__url):
            self.__manga_dir.add_info('url',self.__url)
            self.__manga_dir.add_info('genres',self.__api.genres)
            print('> Pynelo is downloading ',end='')
            manga_img = self.__manga_dir.path / ManganeloScraper.MANGA_IMG_NAME
            if not manga_img.exists():
                self.__download_image(url=self.__api.manga_img,name=manga_img)
            if range:
                range = sorted(range, reverse=True)
                fnumber = lambda d: int(d) if float.is_integer(d) else d
                print(f"from chapters {fnumber(range[0])} to {'the latest' if len(range) == 1 else fnumber(range[1])} of {self.__url}")
                self.__process_range_chapters(range=range,force=force,sleep=sleep)
            elif set_numbers:
                set_numbers = sorted(set_numbers, reverse=True)
                str_set_numbers = list(map(lambda d: str(int(d)) if float.is_integer(d) else str(d),set_numbers))
                print(f'chapters {",".join(str_set_numbers)} of {self.__url}')
                self.__process_set_chapters(set_numbers=set_numbers,force=force,inverse=inverse,sleep=sleep)
            else:
                print(f'all chapters of {self.__url}')
                self.__process_all_chapters(force=force,sleep=sleep)
        self.__manga_dir.add_info('last_chapter',self.__last_chapter)
        self.__manga_dir.update_info()
        print()
