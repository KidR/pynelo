import argparse
from lib.manganelo.api import *
from lib.manganelo.conf import *
from lib.manganelo.scraper import *
from lib.args import PyneloArgs

LOG = FileLogger.get_log()
EXIT_SUCCESS = 0

def __run_vpn_command(cmd):
    vpn_cmd = dict_conf['vpn']['commands'][cmd]
    return proc.check_output(vpn_cmd.split()).decode()

def __connect_vpn(quit_vpn=False):
    if dict_conf.get('vpn') or not quit_vpn:
        if __is_disconnected():
            result = __run_vpn_command('connect')
            print(result)
            LOG.regular(f'CONNECT TO VPN {result[result.index("(")+1:result.index(")")]}')
        else:
            print('> Already connected to VPN : ')
            print(__status_vpn())

def __disconnect_vpn(quit_vpn=False):
    if (dict_conf.get('vpn') and dict_conf['vpn']['kill']) or not quit_vpn:
        print(__run_vpn_command('disconnect'))
        LOG.regular('DISCONNECT TO VPN')

def __status_vpn():
    return __run_vpn_command('status')

def __is_disconnected():
    return dict_conf['vpn']['vars']['disconnected'] in __status_vpn()

def main():
    parser = PyneloArgs()
    parser.parse()
    if parser.show:
        parser.show()
        return EXIT_SUCCESS
    if parser.filter:
        parser.filter['func'](parser.filter['value'])
        return EXIT_SUCCESS
    if parser.search == 0:
        return EXIT_SUCCESS
    if parser.search:
        parser.url = parser.search
    if parser.clear_log:
        LOG.clear()
    __connect_vpn(parser.quit_vpn)
    # Cas 1 : url
    if parser.url:
        scraper = ManganeloScraper(url=parser.url)
        scraper.run(force=parser.force,range=parser.range,set_numbers=parser.set,inverse=parser.inverse,sleep=parser.time)
    # Cas 2 : datafile
    elif parser.import_file:
        for line in parser.import_file:
            parser = PyneloArgs()
            parser.parse(line.split())
            scraper = ManganeloScraper(url=parser.url)
            scraper.run(force=parser.force,range=parser.range,set_numbers=parser.set,inverse=parser.inverse,sleep=parser.time)
    # Cas 3 : update
    elif parser.update:
        for manga_info in parser.update:
            scraper = ManganeloScraper(url=manga_info['url'])
            scraper.run(range=[float(manga_info['last_chapter'])],force=True)
    __disconnect_vpn(parser.quit_vpn)
    return EXIT_SUCCESS


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print(" *** Exiting pynelo.")

