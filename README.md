# README

## Manganelo scraper

A simple command-line app which download scans from [manganelo.com](https://manganelo.com) for a given url.

## Pre-requisites

Go to the pynelo directory `cd /path/to/pynelo/`.

Create your configuration file named `conf.json`.

```json
{
    "logfile": "/path/for/pynelo.log", // path for log with user permission
    "mangas_directory": "/path/to/save/mangas" // path to download manga scans
}
```

If you have a vpn, you can append this to `conf.json`

```json
    "vpn": {
        // needed keys/values
        "commands": {
            // shell commands
            "connect": "nordvpn connect",
            "disconnect": "nordvpn disconnect",
            "status": "nordvpn status"
        },
        "vars": {
            // vars to identify vpn status
            "disconnected": "Status: Disconnected",
            "connected": "Status: Connected"
        },
        "kill": false // true to disconnect vpn at the end else false
    }
```

If you have a webviewer, you can append this too

```json
"viewer": {
        "files":   [".functions.php",".style.css"],
        "template": ".template",
        "directory": "/path/to/save/mangas"
    }
```

Then, run the following command `pip install -r requirements.txt` to install all needed dependecies.

**Important**: for the `logfile` value, make sure you have user permission owner/group.

## Overview

Command : `python3 main.py MANGANELO_URL [OPTIONS]`

Options: 
- `-u <manga_path>` : update manga by downloading the latest missing chapters.
- `-r cmin[,cmax]` : define chapter's range (int or double) to download.
- `-s chap1[,...,chapN]` : download only chapters numbers in set.
- `-t <seconds>` : sleep `<seconds>` between each chapter\'s download.
- `-i <datafile>` : import `<datafile>` which contains all MANGANELO URL to download.
- `--filter <genre>` : show downloaded mangas if corresponding to `<genre>` filter.
- `-f` : force a download if the manga directory is not empty.
- `-q` : ignore vpn config from configuration file.
- `-C` : clear log file if exists in configuration file.
- `-I` : download every chapters numbers except those in set.
- `--show` : show all downloaded mangas.
- `-h` : display help.

## How to

1.  Go to [manganelo.com](https://manganelo.com)
2.  Search the manga you are looking for (*kimetsu no yaiba* yall know)

### download a single chapter

3.  Select the chapter you are interested in
4.  Copy the URL
5.  Run the following command `python3 main.py https://manganelo.com/chapter/kimetsu_no_yaiba/chapter_196`

### download all chapters

3.  Copy the URL
4.  Run the following command `python3 main.py https://manganelo.com/manga/kimetsu_no_yaiba`

### download from chapter `cmin=3` to the latest

3.  Copy the URL
4.  Run the following command `python3 main.py https://manganelo.com/manga/kimetsu_no_yaiba -r 3`

### download from chapter `cmin=3` to chapter `cmax=6.4` *(if exists)*

3.  Copy the URL
4.  Run the following command `python3 main.py https://manganelo.com/manga/kimetsu_no_yaiba -r 3 6.4`

### download chapters 2, 4 and 5 *(if exists)*

3. Copy the URL
4. Run the following command `python3 main.py https://manganelo.com/manga/kimetsu_no_yaiba -s 2 4 5`

### download all chapters except 2, 4 and 5 *(if exists)*

3. Copy the URL
4. Run the following command `python3 main.py https://manganelo.com/manga/kimetsu_no_yaiba -s 2 4 5 -I`

### download mangas from a file

1. Create a file `my_file` 
2. Paste manganelo.com mangas or chapters URL with(out) options *(`-q`,`-h`,`-i` excluded)* separated by a new line.
3. Run the following command `python3 main.py -i my_file`

## update manga

1. Run the following command `python3 main.py -u path/to/manga`

**Important**: scraping can cause <u>a server ban</u>. 