#!/bin/bash

mangas_dir=`cat conf.json | python3 -c 'import sys, json; print(json.load(sys.stdin)["mangas_directory"])'`
manga_url="https://manganelo.com/manga/cu920987";
manga_name="my_recently_hired_maid_is_suspicious";
file_error="tests/err";
file_result="tests/results.test";

run(){
    python3 main.py $args 2> $file_error >/dev/null;
}

tearDown(){
    rm -rf "$mangas_dir/$manga_name";
    rm -f $file_error;
}

testcase(){
    local test
    echo "# $title: pynelo $args";
}

execute(){
    test=$(testcase);
    echo -ne "$test\r";
    echo -ne $test >> $file_result;
    run;
    if [ `cat $file_error | wc -l` -eq 0 ];
    then
        echo -e "\e[32m$test\e[39m";
        echo -e " -> OK" >> $file_result;
    else
        echo -e "\e[91m$test\e[39m";
        echo -e " -> KO" >> $file_result;
        cat $file_error;
    fi
    tearDown;
}

date > $file_result
echo "+---------- TEST SCANPER ----------+";
# Test 1 : download single chapter
title="Download single chapter";
args="https://manganelo.com/chapter/cu920987/chapter_1";
execute;
# Test 2 : download manga with --range and --force
title="Download manga with --range and --force";
args="$manga_url -r 2 3 -f";
execute;
# Test 3 : download manga with --set and --sleep
title="Download manga with --set and --sleep";
args="$manga_url -s 2 4 -t 5";
execute;
# Test 4 : download manga with --set and --inverse
title="Download manga with --set and --inverse";
args="$manga_url -s 1 2 3 4 5 6 7 8 9 10 11 13 -I";
execute;
# Test 5 : download manga with --import
title="Download manga with --import";
args="-i tests/data";
execute;
# Test 6 : download the latest missing chapters with --update
title="Download the latest missing chapters with --update";
args="https://manganelo.com/chapter/cu920987/chapter_12";
run;
args="-u $mangas_dir/$manga_name";
execute;